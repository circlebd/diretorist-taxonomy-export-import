<?php

/**
 * @package  Directorist - Export Import Taxonomies
 */

//ATBDP_DIRECTORY_TYPE

function directorist_save_uploaded_taxonomy_from_csv($taxonomy_name, $csvFile)
{

    $label = array();
    $x = 0;
    $inserted_terms = array();
    $taxonomy_name_o = 'taxonomy';
    if ($taxonomy_name == 'at_biz_dir-category') $taxonomy_name_o = 'category';
    if ($taxonomy_name == 'at_biz_dir-location') $taxonomy_name_o = 'location';

    $dir_types = dir_exim_get_directory_types();

    while (($data = fgetcsv($csvFile)) !== FALSE) {
        if (!empty($data) && count($data) > 0) {
            if ($x == 0) {
                $label = $data;
            } else {
                $tax_data = array();
                foreach ($data as $key => $value) {
                    if ($key == 0) {
                        $tax_data['name'] = $value;
                    } else {
                        $tax_data[$label[$key]] = $value;
                    }
                }


                if (count($tax_data) > 0) {
                    if (!term_exists($tax_data['name'], $taxonomy_name)) {
                        $term_args = array();
                        $term_args['parent'] = 0;

                        /**
                         * Deal With Parent
                         */
                        if (isset($tax_data['parent']) && !empty($tax_data['parent'])) {
                            if (isset($inserted_terms[$tax_data['parent']]) && !empty($inserted_terms[$tax_data['parent']])) {
                                $term_args['parent'] = $inserted_terms[$tax_data['parent']];
                            } else {
                                $parent_term = get_term_by('name', $tax_data['parent'], $taxonomy_name);
                                if ($parent_term) {
                                    $term_args['parent'] = $parent_term->term_id;
                                }
                            }
                        }

                        /**
                         * Deal with Directory Types
                         */
                        $term_directory_types = array();
                        if (!empty($dir_types) && isset($tax_data['directory_type']) && !empty($tax_data['directory_type'])) {
                            $directory_types = explode(',', $tax_data['directory_type']);

                            if (!empty($directory_types) && count($directory_types) > 0) {
                                foreach ($directory_types as $directory_type) {
                                    if (!empty($dir_types && count($dir_types))) {
                                        foreach ($dir_types as $key => $type) {
                                            if ($type == trim($directory_type)) $term_directory_types[] = $key;
                                        }
                                    }
                                }
                            }
                        }


                        if (isset($tax_data['description']) && !empty($tax_data['description'])) $term_args['description'] = $tax_data['description'];
                        if (isset($tax_data['slug']) && !empty($tax_data['slug'])) $term_args['slug'] = $tax_data['slug'];

                        $term_data = wp_insert_term(
                            $tax_data['name'],   // the term 
                            $taxonomy_name, // the taxonomy
                            $term_args
                        );

                        if (!is_wp_error($term_data) && isset($term_data['term_id']) && !empty($term_data['term_id'])) {
                            $inserted_terms[$tax_data['name']] = $term_data['term_id'];
                            if ($taxonomy_name == 'at_biz_dir-category' && isset($tax_data['icon']) && !empty($tax_data['icon'])) update_term_meta($term_data['term_id'], 'category_icon', $tax_data['icon']);
                            if (!empty($term_directory_types) && count($term_directory_types) > 0) update_term_meta($term_data['term_id'], '_directory_type', $term_directory_types);
                        }
                    }
                }
            }
        }
        $x++;
    }

    //e_var_dump($label);
    //e_var_dump($inserted_terms);

    echo "<h4 style='color: green;'>Total " . $taxonomy_name_o . " inserted : " . count($inserted_terms) . "</h4>";
}


// CSV EXPORT

if (isset($_GET['action']) && $_GET['action'] == 'download_csv') {
    // Handle CSV Export
    add_action('admin_init', 'direcorist_taxonomy_csv_export');
}

function direcorist_taxonomy_csv_export()
{

    // Check for current user privileges 
    if (!current_user_can('manage_options')) {
        return false;
    }

    // Check if we are in WP-Admin
    if (!is_admin()) {
        return false;
    }

    // Nonce Check
    $nonce = isset($_GET['_wpnonce']) ? $_GET['_wpnonce'] : '';
    if (!wp_verify_nonce($nonce, 'download_csv')) {
        die('Security check error');
    }

    $taxonomy_name = isset($_GET['taxonomy']) && !empty($_GET['taxonomy']) ? $_GET['taxonomy'] : 'at_biz_dir-category';

    ob_start();

    $domain = $_SERVER['SERVER_NAME'];

    if ($taxonomy_name == 'at_biz_dir-category') {
        $header_row = array("name", "slug", "icon", "description", "parent", "directory_type");
        $filename = 'categories.csv';
    } else if ($taxonomy_name == 'at_biz_dir-location') {
        $header_row = array("name", "slug", "description", "parent", "directory_type");
        $filename = 'locations.csv';
    }

    $dir_types = dir_exim_get_directory_types();

    $data_rows = array();

    $term_list = get_terms(array(
        'taxonomy' => $taxonomy_name,
        'hide_empty' => false,
        'orderby' => 'parent',
    ));

    if ($term_list && count($term_list) > 0) {
        foreach ($term_list as $term) {

            //Deal with parent
            $term_parent = '';
            if ($term->parent) {
                $parent_term = get_term_by('id', $term->parent, $taxonomy_name);
                if ($parent_term) {
                    $term_parent = wp_specialchars_decode($parent_term->name);
                }
            }

            //Deal with directory type
            $term_dir_types = array();
            $directory_types = get_term_meta($term->term_id, '_directory_type', true) ? get_term_meta($term->term_id, '_directory_type', true) : '';

            if (!empty($dir_types) && !empty($directory_types)) {
                foreach ($directory_types as $directory_type) {
                    $term_dir_types[] = $dir_types[$directory_type];
                }
            }

            $term_dir_types = count($term_dir_types) > 0 ? implode(',', $term_dir_types) : '';

            //Deal with Icon
            if ($taxonomy_name == 'at_biz_dir-category') {
                $term_icon = get_term_meta($term->term_id, 'category_icon', true) ? get_term_meta($term->term_id, 'category_icon', true) : '';
                $row = array(
                    wp_specialchars_decode($term->name),
                    wp_specialchars_decode($term->slug),
                    $term_icon,
                    wp_specialchars_decode($term->description),
                    $term_parent,
                    $term_dir_types
                );
            } else {
                $row = array(
                    wp_specialchars_decode($term->name),
                    wp_specialchars_decode($term->slug),
                    wp_specialchars_decode($term->description),
                    $term_parent,
                    $term_dir_types
                );
            }

            $data_rows[] = $row;
        }
    }

    $fh = @fopen('php://output', 'w');
    fprintf($fh, chr(0xEF) . chr(0xBB) . chr(0xBF));
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Content-Description: File Transfer');
    header('Content-type: text/csv');
    header("Content-Disposition: attachment; filename={$filename}");
    header('Expires: 0');
    header('Pragma: public');
    fputcsv($fh, $header_row);

    foreach ($data_rows as $data_row) {
        fputcsv($fh, $data_row);
    }
    fclose($fh);

    ob_end_flush();

    die();
}


function dir_exim_get_directory_types()
{
    $dir_types = array();
    $directory_types = get_terms(array(
        'taxonomy'   => ATBDP_TYPE,
        'hide_empty' => false,
    ));

    if (!is_wp_error($directory_types) && count($directory_types) > 0) {
        foreach ($directory_types as $dir_type) {
            $dir_types[$dir_type->term_id] = $dir_type->name;
        }
    }

    if (!empty($dir_types)) {
        return $dir_types;
    } else {
        return false;
    }
}
